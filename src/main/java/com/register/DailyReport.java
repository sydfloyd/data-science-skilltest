package com.register;


import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.register.models.reports.in.LogRecord;
import com.register.models.reports.out.ReportRecord;

/**
 * This class is a java native test for Register.it selection.
 *
 * @author tcatalano
 * Specs:
 *  1. run as java app to test
 *  2. camelCase syntax notation
 *  3. small maven configuration as requires
 */
public class DailyReport {

    private static final Logger LOGGER = Logger.getLogger(DailyReport.class);

    /**
     * main
     *
     * @param args
     */
    public static void main(String args[]) {

        // total requests
        final List<LogRecord> logRecords = logRecords();
        if (CollectionUtils.isEmpty(logRecords)) {
            throw new RuntimeException(" LogRecords is null");
        }

        try {
            // total bytes sent
            final BigDecimal totalBytesSent = logRecords.stream().map(record -> record.getBytesSent())
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            // requests by IP
            final Map<String, List<LogRecord>> requestsByIp = recordsByIp();

            // group by IP
            final List<ReportRecord> reportRecords = requestsByIp
                    .keySet()
                    .stream()
                    .map(k -> {
                        // current ip
                        final List<LogRecord> ip = requestsByIp.get(k);
                        // bytes sent per IP
                        final BigDecimal bytesSent = ip.stream().map(key -> key.getBytesSent())
                                .reduce(BigDecimal.ZERO, BigDecimal::add);
                        // create output record for current ip
                        return new ReportRecord(k, BigInteger.valueOf(ip.size()),
                                (((float) ip.size() / logRecords.size()) * 100), bytesSent,
                                bytesSent.divide(totalBytesSent, 2, RoundingMode.HALF_UP)
                                        .multiply(new BigDecimal(100)).floatValue());
                    }).collect(Collectors.toList());
            // report direcotory
            Path path = pathReportsFile();
            // write report
            writeReport(path, reportRecords);
        } catch (Exception e) {
            LOGGER.error(" Test Failed for this reason ", e);
        }
    }

    /**
     * report file path.
     *
     * @return
     */
    private static Path pathReportsFile() {

        try {
            InputStream propertiesStream = ClassLoader.getSystemResource("config.properties").openStream();
            Properties properties = new Properties();
            properties.load(propertiesStream);
            String reportsDir = properties.getProperty("skilltest.reports.path");
            return Paths.get(reportsDir + ReportRecord.REPORT_NAME).toAbsolutePath();
        } catch (Exception e) {
            LOGGER.error(" Unable to create file ", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * write a csv detailts to report file.
     *
     * @param path
     * @param reportRecords
     */
    private static void writeReport(Path path, List<ReportRecord> reportRecords) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(path);
            writer.write(ReportRecord.csvHeader());
            writer.newLine();
            LOGGER.info(" Generating " + ReportRecord.REPORT_NAME + " in resources path ");
            reportRecords.stream()
                    .map(ReportRecord::toString)
                    .forEach(ip -> {
                        try {
                            writer.write(ip);
                            writer.newLine();
                        } catch (IOException e) {
                            LOGGER.warn(" Error during append report for curret ip " + ip);
                        }
                    });
            writer.close();
        } catch (Exception e) {
            LOGGER.error(" eorro ", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Records by IP filtering http status 200.
     */
    private static Map<String, List<LogRecord>> recordsByIp() {

        try {
            // stream
            final Map<String, List<LogRecord>> collect = logLines()
                    .stream()
                    .map(record -> new LogRecord(record))
                    .filter(record -> LogRecord.STATUS_OK.equalsIgnoreCase(record.getHttpStatus()))
                    .collect(Collectors.groupingBy(logRecord -> logRecord.getClientIp()));
            LOGGER.error(" ");
            return collect;
        } catch (Exception e) {
            LOGGER.error(" Input filePath not valid. Check the filePath ", e);
        }
        return null;
    }


    /**
     * All records from loog.
     *
     * @return
     */
    private static List<LogRecord> logRecords() {

        try {
            // stream
            final List<LogRecord> collect = logLines()
                    .stream()
                    .map(record -> new LogRecord(record))
                    .collect(Collectors.toList());
            LOGGER.error(" Error ");
            return collect;
        } catch (Exception e) {
            LOGGER.error(" Input filePath not valid. Check the filePath ", e);
        }
        return null;
    }

    /**
     * return a log file as list of String.
     *
     * @return
     */
    private static List<String> logLines() {

        Path fileLogPath = null;
        try {
            fileLogPath = Paths.get(DailyReport.class.getClassLoader()
                    .getResource("logfiles/requests.log").toURI());
            return Files.lines(fileLogPath)
                    .collect(Collectors.toList());
        } catch (URISyntaxException e) {
            LOGGER.error(" PAth malformed. Check the path resource ", e);
            throw new RuntimeException(e);
        } catch (IOException ioE) {
            LOGGER.error(" File not found! ", ioE);
        }
        return null;
    }

}
