package com.register;

import java.util.Arrays;

/**
 * I'm assume that the all array are degenerate, thus it contains only one elem.
 *
 *  This algorithm has complexity like this: O(n^2):
 */
public class MultiplyOperator {

    private static int [] n1 = {100};
    private static int [] n2 = {10};
    private static int [] n3 = {30};

    /**
     * main
     * @param args
     */
    public static void main(String[] args) {

        multiplyAsCombinatorSum(MultiplyOperator.n1, MultiplyOperator.n2);
    }

    private static void multiplyAsCombinatorSum(int[] array1, int[] array2) {

        int firsNumber = array1[0];
        int secondNumber = array2[0];

        // I'm create a 2 list
        int [] firstNumberAsVoidArray = new int[firsNumber];
        int [] arrayOf1FirstNumer = createArrayOf0(firstNumberAsVoidArray);

        int [] secondNumberAsVoidArray = new int[secondNumber];
        int [] arrayOf1SecondNumer = createArrayOf1(secondNumberAsVoidArray);

        long sum = 0;
        for (int i = 0; i < arrayOf1FirstNumer.length; i++) {
            int currentSum = arrayOf1FirstNumer[i];
            for (int j = 0; j < arrayOf1SecondNumer.length; j++) {
                currentSum += arrayOf1SecondNumer[j];
            }
            sum += currentSum;
        }
        System.out.println("The multiplication of n1 and n2 is:  " + String.valueOf(sum));
    }

    /**
     * Create an array that contains only "1" digit. The size of this array is equals to input params @array
     *
     * @param array
     * @return
     */
    private static int[] createArrayOf1(int [] array) {

        for (int i = 0; i < array.length; i++) {
            array[i] = 1;
        }
        return array;
    }

    /**
     * This represents the neutral-array for additional operation.
     *
     * @param array
     * @return
     */
    private static int[] createArrayOf0(int [] array) {

        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }
        return array;
    }


}
