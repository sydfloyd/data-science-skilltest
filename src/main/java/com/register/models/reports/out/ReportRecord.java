package com.register.models.reports.out;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ReportRecord {

  private String ip;

  private BigInteger nRequests;

  private float requestsRate;

  private BigDecimal bytesSent;

  private float bytesRate;

  public static final String REPORT_NAME = "ipaddr.csv";

  public ReportRecord(String ip, BigInteger nRequests, float requestsRate,
      BigDecimal bytesSent, float bytesRate) {

    this.ip = ip;
    this.nRequests = nRequests;
    this.requestsRate = requestsRate;
    this.bytesSent = bytesSent;
    this.bytesRate = bytesRate;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public BigInteger getnRequests() {
    return nRequests;
  }

  public void setnRequests(BigInteger nRequests) {
    this.nRequests = nRequests;
  }

  public float getRequestsRate() {
    return requestsRate;
  }

  public void setRequestsRate(float requestsRate) {
    this.requestsRate = requestsRate;
  }

  public BigDecimal getBytesSent() {
    return bytesSent;
  }

  public void setBytesSent(BigDecimal bytesSent) {
    this.bytesSent = bytesSent;
  }

  public float getBytesRate() {
    return bytesRate;
  }

  public void setBytesRate(float bytesRate) {
    this.bytesRate = bytesRate;
  }

  @Override
  public String toString() {
    return this.ip + "," +
        this.nRequests + "," +
        this.requestsRate + "%," +
        this.bytesSent + "," +
        this.bytesRate + "%";
  }

  public static String csvHeader() {
    return "IP Address,Number of Requests,Percentage of the total amount of requests,Total Bytes sent,Percentage of the total amount of bytes";
  }
}
