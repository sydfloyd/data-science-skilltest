package com.register.models.reports.in;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;

public class LogRecord {

  public static final Logger LOGGER = Logger.getLogger(LogRecord.class);

  private LocalDateTime timestamp;

  private BigDecimal bytesSent;

  private String httpStatus;

  private String clientIp;

  public static final String STATUS_OK = "OK";

  public static final String PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

  private LogRecord() {
  }

  public LogRecord(String record) {

    // split IN record
    try {
      // try with horizontal tabulation
      String[] split = record.split("\\s{4}");
      buildLogRecord(
          LocalDateTime.parse(split[0], DateTimeFormatter.ofPattern(PATTERN)),
          new BigDecimal(split[1]), split[2], split[3]);
    } catch (Exception e) {
      LOGGER.error(" Record malformed. Redefine Specification! ", e);
    }
  }

  private void buildLogRecord(LocalDateTime timestamp, BigDecimal bytesSent, String httpStatus,
      String clientIp) {

    this.timestamp = timestamp;
    this.bytesSent = bytesSent;
    this.httpStatus = httpStatus;
    this.clientIp = clientIp;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public BigDecimal getBytesSent() {
    return bytesSent;
  }

  public String getHttpStatus() {
    return httpStatus;
  }

  public String getClientIp() {
    return clientIp;
  }
}

